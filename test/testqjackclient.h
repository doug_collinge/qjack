#ifndef TESTQJACKCLIENT_H
#define TESTQJACKCLIENT_H

#include <QtTest/QtTest>

class TestQJackClient : public QObject
{
    Q_OBJECT

public:
    TestQJackClient();

private slots:
     void create();
};

#endif // TESTQJACKCLIENT_H
