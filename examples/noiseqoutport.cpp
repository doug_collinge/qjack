#include "noiseqoutport.h"

NoiseQOutPort::NoiseQOutPort(QString name, quint32 bufferSize, QObject *parent):
    QJack::QOutPort(name,bufferSize,parent)
{
}

void NoiseQOutPort::setData(void *data, quint32 nFrames)
{
    float *fdata = (float *)data;

    // We need nFrames samples of data.
    for(quint32 i=0; i<nFrames;i++)
    {
        // Generate a single sample of random data.
        fdata[i] = ( ((float)(std::rand()) )/RAND_MAX)*2 - 1.;
    }
}
