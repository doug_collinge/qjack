#ifndef EXPONENTIALFILTER_H
#define EXPONENTIALFILTER_H

#include "math.h"

/*! \brief An exponential filter
 * This exponential filter is a simple filter like an analog RC filter.
 * It has a time constant and the sampling rate as parameters.
 */
class ExponentialFilter
{
public:
    /*! \brief Basic constructor.
     * \param sampleRate The system sampling rate, must not change.
     * \param timeConstant The filter time constant.
     */
    ExponentialFilter(float sampleRate, float timeConstant);

    /*! \brief Compute one sample of the filter.
     * \param xn the incoming sample.
     * \return yn the filter output.
     */
    float filter(float xn);

private:
    float a;    // Filter constant.
    float ynm1; // Filter state variable.

};

#endif // EXPONENTIALFILTER_H
