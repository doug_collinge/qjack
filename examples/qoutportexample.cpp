#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <math.h>
#include <QtCore/QDebug>
#include <QStringList>

#include "qjack/qjackclient.h"
#include "qjack/qsignalinport.h"
#include "qjack/qinport.h"
#include "qjack/qoutport.h"
#include "jack/types.h"


#include "noiseqoutport.h"
#include "toneqoutport.h"

using namespace std;
using namespace QJack;

int main(int argc, char** argv)
{
    cout<<"QOutPortExample starting."<<endl;

    // Allocate a Jack client and name it.
    QJackClient* client = new QJackClient("QOutPortExample");

    // Open the client or bust.
    if (false == client->open()){
        exit(EXIT_FAILURE);
    }

    // Create a noise port with a buffer with 2 * number of frames jack will pass me each time
    QOutPort *noise_port = new NoiseQOutPort("noise", client->nFrames() * 2, client);
    client->addPort(noise_port);

    // Similarly, open a tone port, giving sample rate and frequency of tone.
    QOutPort *tone_port = new ToneQOutPort("tone", client->nFrames() * 2, client->sampleRate(),400.,client);
    client->addPort(tone_port);

    // Get the client off the runway.
    client->activate();

    cout << "All ports available in JACK server:"<<endl;
    QStringList portnames = client->getPorts(NULL,NULL,(JackPortFlags)0);

    for(int ipnames = 0;ipnames<portnames.size();ipnames++)
    {
        cout << ipnames << ": " << portnames[ipnames].toStdString() << endl;
    }
    cout << endl;

    cout << "Connecting newly created output ports to system input ports." << endl;
    QStringList inportnames = client->getPorts( NULL, NULL, (JackPortFlags)(JackPortIsInput|JackPortIsPhysical) );

    if( inportnames.size() > 0 )    // Make sure there is at least one system input port.
    {
        if( 0 != client->connect(noise_port,inportnames[0]) )   // Make the connection.
        {
            cout << "Failed to connect noise port to system in port." << endl;
        }
    }
    else
    {
        cout << "No input port available for noise port." << endl;
    }

    if( inportnames.size() > 1 )    // Now make sure there is another port.
    {
        if( 0 != client->connect(tone_port,inportnames[1]) )    // Make the connection.
        {
            cout << "Failed to connect tone port to system in port." << endl;
        }
    }
    else
    {
        cout << "No port available for tone port." << endl;
    }
    cout<<endl;

    cout<<"Entering background, JACK should now be making sound."<<endl;

    cout<<"press [enter] to exit ..."<<endl;
    cin.get();

    cout<<"Done. Closing."<<endl;
    client->deactivate();
    client->close();

    delete(client);     // Client is a QObject so memory should be automatically mopped up.

    return (0);
}
