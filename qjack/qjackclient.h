/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QJACKCLIENT_H
#define QJACKCLIENT_H

extern "C" {
#include <jack/jack.h>
#include <jack/transport.h>
}

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QStringList>

#include "jackport.h"
#include "qinport.h"
#include "qoutport.h"

namespace QJack {

//! This class will connect to the jack server and it will create the ports you want
/*!
 * The sequence of use is:
 * open();
 * addPort();
 * activate();
 * deactivate();
 * close();
 */
//class QJackClientPrivate;
class QJackClient : public QObject
{
    Q_OBJECT

public:
    //! The constructor
    /*!
     * \param name the name of the client in the jack bus
     */
    explicit QJackClient(QString name, QObject *parent = 0);
    virtual ~QJackClient();

    //! errno
    // local storage of the error number returned (if any)
    // from functions called made available for reference or display.
    int errno;

    //! errstr
    // local storage the error message (if any)
    // from functions called made available for reference or display.
    QString errstr;

    //! it opens the connection with the jack server
    bool open();
    
    //! it actives the client in the jack bus
    bool activate();
    
    //! it deactives the client in the jack bus
    bool deactivate();
    
    //! it closes the connections with the jack bus
    bool close();

    bool addPort(JackPort *port);

    //! Get ports meeting criteria. Refer to jack_get_ports().
    QStringList getPorts(QString portnamepattern, QString typenamepattern, JackPortFlags flags);

    //! Connect up ports, given their names. Refer to jack_connect().
    int connect(const QString &source, const QString &destination);
    int connect(JackOutPort *source, const QString &destination);
    int connect(const QString &source, JackInPort *destination);
    int connect(JackOutPort* source, JackInPort* destination);

    //! It returns the number of frames jack will pass each time
    quint32 nFrames() const;

    //! It returns the current frameRate
    quint32 sampleRate() const;
    
      QString fullPortName(JackPort* port);
//    QString fullPortName(QInPort port);
//    QString fullPortName(QOutPort port);

private:
    QList<JackPort *> m_ports;
    quint32 m_nFrames;
    quint32 m_sampleRate;

    QString m_clientName;

    jack_client_t *m_client;
    jack_status_t m_status;

    bool checkStatus(JackStatus value);
    bool openConnection();
    bool setCallbacks();
    static int jackBufferSizeCallback(jack_nframes_t nframes, void *arg);
    static void jack_shutdown(void *arg);
    static int process(jack_nframes_t nframes, void *arg);
    bool setFeatures();
};

} // namspace

#endif // QJACKCLIENT_H
