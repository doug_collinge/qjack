/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "qjackclient.h"
#include <QtCore/QDebug>

namespace QJack {

QJackClient::QJackClient(QString name, QObject *parent): QObject(parent)
{
    m_clientName = name;
    m_client = 0;
}

QJackClient::~QJackClient()
{
}

bool QJackClient::open()
{

    if (m_client)
        return(false);

    // opening a connection with the jack server
    if (!openConnection()) return(false);

    // ask for the buffers features
    if (!setFeatures()) return(false);

    // setting the callbacks
    if (!setCallbacks()) return(false);

    return(true);
}

quint32 QJackClient::nFrames() const
{
    return(m_nFrames);
}

quint32 QJackClient::sampleRate() const
{
    return(m_sampleRate);
}

bool QJackClient::activate()
{
    if (!m_client)
    {
        return(false);
    }

    if (m_ports.size() == 0)
    {
        qWarning() << "QJackClient | Can't activate a client without ports";
        return(false);
    }

    if (jack_activate (m_client)) {
        qWarning() << "QJackClient | cannot activate client";
        return(false);
    }
    return(true);
}

bool QJackClient::deactivate()
{
    if (!m_client) {
        qWarning() << "QJackClient | deactive .. client isn'ty opened";
        return(false);
    }

    if (jack_deactivate(m_client)) {
        qWarning() << "QJackClient | deactivation doesn't work";
        return(false);
    }

    return(true);
}

bool QJackClient::close()
{
    if (!m_client) {
        qWarning() << "QJackClient | close .. client isn'ty opened";
        return(false);
    }

    if (jack_client_close(m_client)) {
        qWarning() << "QJackClient | closing doesn't work";
        return(false);
    }

    m_client = 0;
    return(false);
}

bool QJackClient::addPort(JackPort *port)
{
    if (!port) {
        qWarning() << Q_FUNC_INFO << ": null port";
        return(false);
    }

    qDebug() << "QJackClient | trying to add port = " << port->name() << " type = " << port->m_type;

    port->m_port = jack_port_register (m_client, port->name().toStdString().c_str(),
                                         JACK_DEFAULT_AUDIO_TYPE,
                                         port->m_type, 0);

    // check if port was created
    if (!port->m_port) {
        qWarning() << "QJackClient | no ports available";
        return(false);
    }

    m_ports.append(port);

    return(true);
}

QStringList QJackClient::getPorts(QString portnamepattern, QString typenamepattern, JackPortFlags flags)
{
    const char** pnames = jack_get_ports(m_client,portnamepattern.toStdString().c_str(),typenamepattern.toStdString().c_str(),flags);

    QStringList qslnames;   // To be returned.

    char** tpnames = (char**)pnames;
    while(*tpnames)
    {
        qslnames.append(*tpnames);
        tpnames++;
    }
    return( qslnames );
}

QString QJackClient::fullPortName(JackPort *port)
{
    return QString(m_clientName).append(":").append(port->name());
}


int QJackClient::connect(const QString &source, const QString &destination)
{
    qDebug() << "QJackClient | trying to connect: " << (source.toStdString().c_str()) << " to: " << (destination.toStdString().c_str()) << endl;
    return jack_connect(m_client, source.toStdString().c_str(), destination.toStdString().c_str());
}

int QJackClient::connect(JackOutPort* source, const QString &destination)
{
    qDebug() << "QJackClient | trying to connect: " << (fullPortName(source).toStdString().c_str()) << " to: " << (destination.toStdString().c_str()) << endl;
    return jack_connect(m_client, fullPortName(source).toStdString().c_str(), destination.toStdString().c_str());
}

int QJackClient::connect(const QString &source, JackInPort* destination)
{
    qDebug() << "QJackClient | trying to connect: " << (source.toStdString().c_str()) << " to: " << (fullPortName(destination).toStdString().c_str()) << endl;
    return jack_connect(m_client, source.toStdString().c_str(), fullPortName(destination).toStdString().c_str());
}

int QJackClient::connect(JackOutPort* source, JackInPort* destination)
{
    qDebug() << "QJackClient | trying to connect: " << (fullPortName(source).toStdString().c_str()) << " to: " << (fullPortName(destination).toStdString().c_str()) << endl;
    return jack_connect(m_client, fullPortName(source).toStdString().c_str(), fullPortName(destination).toStdString().c_str());
}


bool QJackClient::setFeatures()
{
    m_nFrames = jack_get_buffer_size(m_client);
    m_sampleRate = jack_get_sample_rate(m_client);

    qDebug() << "QJackClient | client(" << m_clientName << ") nFrames(" << m_nFrames << ") " << "sampleRate(" << m_sampleRate << ")";

    return(true);
}

int QJackClient::process (jack_nframes_t nframes, void *arg)
{
    //    qDebug() << Q_FUNC_INFO << ": frames = " << nframes;
    const QJackClient *that = reinterpret_cast<const QJackClient*>(arg);

    for (int i = 0 ; i < that->m_ports.size() ; i++) {
        JackPort *qJPort = that->m_ports[i];
        void *buffer = jack_port_get_buffer (qJPort->m_port, nframes);
        qJPort->setData(buffer, nframes);
    } // for

    return(0);
}

void QJackClient::jack_shutdown (void *arg)
{
    qWarning() << Q_FUNC_INFO << ": called ... doing nothing";
}


int QJackClient::jackBufferSizeCallback(jack_nframes_t nframes, void *arg)
{
    qDebug() << Q_FUNC_INFO << ": nframes = " << nframes;
    return(0);
}

bool QJackClient::setCallbacks()
{
    /* tell the JACK server to call `process()' whenever
         there is work to be done.
      */
    if (jack_set_process_callback (m_client, process, this))
    {
        qWarning() << "jack_set_process_callback : failed";
        return(false);
    }

    if (jack_set_buffer_size_callback(m_client, jackBufferSizeCallback, this)) {
        qWarning() << "jack_set_buffer_size_callback: failed";
        return(false);
    }

    /* tell the JACK server to call `jack_shutdown()' if
       it ever shuts down, either entirely, or if it
       just decides to stop calling us.
    */
    jack_on_shutdown (m_client, jack_shutdown, this);

    return(true);
}

bool QJackClient::openConnection()
{
    m_client = jack_client_open(m_clientName.toStdString().c_str(), JackNullOption , &m_status);
    if ( !m_client )
    {
        if ( checkStatus(JackServerFailed) ) 
		{
			qWarning() << "QJackClient | " << errno << " " << errstr ;
		 	return false;
		}
    }
    qWarning() << "QJackClient | JACK server found";
    return true;
}

bool QJackClient::checkStatus(JackStatus value)
{
    errno = value;
    switch (value)
    {
    case JackFailure:
        errstr = tr("General Jack Audio Failure");
        break;
    case JackInvalidOption:
        errstr = tr("Invalid Option supplied");
        break;
    case JackNameNotUnique:
        errstr = tr("Name supplied is not unique");
        m_clientName = jack_get_client_name(m_client);
        qDebug() << Q_FUNC_INFO << "unique name " << m_clientName << " assigned";
        break;
    case JackServerStarted:
        errstr = tr("Jack Audio Ready - server started");
        break;
    case JackServerFailed:
        errstr = tr("Jack Server Failed to start");
        qDebug() << Q_FUNC_INFO << "err: " << errno;
        break;
    case JackServerError:
        errstr = tr("Communication error with Jack Server");
        break;
    case JackNoSuchClient:
        errstr = tr("No such Jack client named");
        break;
    case JackLoadFailure:
        errstr = tr("Unable to load internal Jack client");
        break;
    case JackInitFailure:
        errstr = tr("Error initializing Jack Audio Server");
        break;
    case JackShmFailure:
        errstr = tr("Error initializing or accessing shared memory");
        break;
    case JackVersionError:
        errstr = tr("Client protocol version does not match server");
        break;
    case JackBackendError:
        errstr = tr("Unknown server error on backend");
        break;
    case JackClientZombie:
        errstr = tr("Client has stopped communicating and become a zombie");
        break;
    }
    return (m_status & value);
}

} // namespace



