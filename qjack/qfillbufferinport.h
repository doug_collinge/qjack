/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef QFILLBUFFERINPORT_H
#define QFILLBUFFERINPORT_H

#include <QObject>

#include "inport.h"

namespace QJack{

/// \brief QFillBufferInPort signalling QInPort
/// This subclass of QInPort has a slot that accepts a signal
/// that carries a pointer to a buffer with a size.
/// It fills the given buffer and then signals back to say it's full.

class QFillBufferInPort : public QObject, public InPort
{
    Q_OBJECT
public:
    //! \brief QFillBufferInPort Constructor
    //! \param name Name of JACK input port to be created
    //! \param buffersize Size of ringbuffer to allocate
    //! \param parent QObject parent
    //!
    explicit QFillBufferInPort(QString name, quint32 buffersize, QObject *parent = 0);

    ~QFillBufferInPort();
    
private:
    //!
    //! \brief setData Override virtual method from JackPort
    //! \param data pointer to buffer containing data from JACK
    //! \param nFrames number of frames of samples in buffer
    //!
    void setData(void *data, quint32 nFrames);

    //!
    //! \brief fillPartially Fill the fillBuffer as much as possible
    //! Check to see if there is data available in the m_buffer from
    //! InPort and, if so, move it into the fillBuffer. If the fillBuffer
    //! actually gets full then emit the signal.
    //!
    void fillPartially();

    //!
    //! \brief m_buffer A pointer to the buffer we are filling.
    //!
    jack_default_audio_sample_t *m_fillBuffer;

    //!
    //! \brief m_bufferSize Size of the provided buffer.
    //!
    quint32 m_fillBufferSize;

    //!
    //! \brief m_filledFrames The number of frames *m_buffer currently contains.
    //!
    quint32 m_filledFrames;

public slots:
    //!
    //! \brief fill Fill this buffer with frames.
    //! \param buffer Pointer to the buffer to fill.
    //! \param nframes Capacity of the buffer.
    //! When this port gets signaled on this slot it starts filling
    //! the given buffer with data, possible across many setData()
    //! cycles. When it is full it signals to say so and then continues
    //! collecting data into the ringbuffer. The next signal on this
    //! slot will put the data already in the ringbuffer into the buffer
    //! so that nothing is lost.
    void fill(jack_default_audio_sample_t *fillBuffer, quint32 fillBufferSize);

signals:
    //!
    //! \brief filled The signal to fire when buffer is full enough
    //!
    void filled();

};

} // namespace QJack

#endif // QFILLBUFFERINPORT_H

