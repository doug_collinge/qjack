/*
    QJack make you connect with jack soundserver system
    Copyright (C) 2011  Alessandro Siniscalchi <asiniscalchi@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef JACKPORT_H
#define JACKPORT_H

#include <QtCore/QtGlobal>
#include <QtCore/QString>
#include <jack/jack.h>

//! The base class for every jack port
/*! Inherit and implement the protected methods.
 */
namespace QJack {

class JackPort
{
public:
    JackPort(const QString &name, JackPortFlags type);
    virtual ~JackPort();
    QString name() const;

protected:
    //! this function is called by the jack thread
    /*! Implement it to implement your own port
      \param data the buffer that jack provide. It is valid until the function returns
      \param nFrames the number of frames in data
      */
    virtual void setData(void *data, quint32 nFrames) = 0;

private:
    friend class QJackClient;

    QString m_name;
    jack_port_t *m_port;
    JackPortFlags m_type;
};

} // namespace

#endif // QJACKPORT_H

